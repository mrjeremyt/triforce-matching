//
//  PrimaryViewController.m
//  TriForce Match
//
//  Created by Jeremy Thompson on 2/9/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import "PrimaryViewController.h"


@interface PrimaryViewController()
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedMode;




@end


@implementation PrimaryViewController
//@synthesize logFile;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.numGames = 0;
    self.avgScore = 0.0;
    
}

-(void)updateLog{
    NSLog(@"%@", [self.logFile componentsJoinedByString:@"\n"]);
    if(self.game.logEntry == nil) return;
    [self.logFile addObject:self.game.logEntry];
    NSLog(@"What is the logEntry: %@", self.game.logEntry);
    NSLog(@"%@", [self.logFile componentsJoinedByString:@"\n"]);
    self.game.logEntry = nil;
}


- (IBAction)modeSwitch:(id)sender {
}


- (IBAction)resetWasClicked:(id)sender{
    
}


- (IBAction)peekStateChange:(id)sender {
}


-(IBAction)click:(id)sender{
    

    
}




- (IBAction)click:(UIButton *)sender
          theCard:(RSCard *)card
          theBool:(BOOL)up {
    
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
