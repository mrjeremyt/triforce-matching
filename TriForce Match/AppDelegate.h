//
//  AppDelegate.h
//  TriForce Match
//
//  Created by Jeremy Thompson on 2/9/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

