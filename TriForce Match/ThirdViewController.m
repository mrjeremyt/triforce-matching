//
//  ThirdViewController.m
//  TriForce Match
//
//  Created by Jeremy Thompson on 2/9/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import "ThirdViewController.h"
#import "test.h"
#import "ExpandedPlayingCard.h"

@interface ThirdViewController()
@property (weak, nonatomic) IBOutlet UINavigationItem *navBarForScore;
@property (weak, nonatomic) IBOutlet UISegmentedControl *matchingModeSelector;
@property (weak, nonatomic) IBOutlet UIButton *peekButton;
@property (weak, nonatomic) IBOutlet UIButton *resetButton;
@end

@implementation ThirdViewController

@synthesize game = _game;
@synthesize logFile = _logFile;
-(CardMatchingGame *)game{
    if(!_game){
        _game = [[CardMatchingGame alloc] initWithInt:3];
        _game.delegate = self;
    }
    _logFile = _game.logFile;
    return _game;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navBarForScore setTitle:[NSString stringWithFormat:@"Score: %0.0f", (self.game.currentScore)]];
    // Do any additional setup after loading the view, typically from a nib.
//    _logFile = [[NSMutableArray alloc] init];
}

-(void)viewWillAppear:(BOOL)animated{
    [self.navBarForScore setTitle:[NSString stringWithFormat:@"Score: %0.0f", (self.game.currentScore)]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)peekStateChange:(id)sender{
    UIButton *b = sender;
    if(self.game.peekUp == true){
        [self.game peekIsUp:false];
        [b setTitle:@"Peek" forState:UIControlStateNormal];
        self.matchingModeSelector.userInteractionEnabled = true;
        [self.game peek:NO];
    }
    else{
        [self.game peekIsUp:true];
        [b setTitle:@"Un-Peek" forState:UIControlStateNormal];
        self.matchingModeSelector.userInteractionEnabled = false;
        [self.game peek:YES];
        self.game.currentScore -= 15;
    }
    [self.navBarForScore setTitle:[NSString stringWithFormat:@"Score: %0.0f", (self.game.currentScore)]];
    NSLog(@"changing the peek bool: %d", self.game.peekUp);
}




- (IBAction)modeSwitch:(id)sender {
    int index = (int)((UISegmentedControl *)sender).selectedSegmentIndex;
    if (index == 0) {
        [self.game changeToThreeCardMatching:NO];
    }else if(index == 1){
        [self.game changeToThreeCardMatching:YES];
    }else{
        //really bad news
    }
}







- (IBAction)resetWasClicked:(id)sender {
    [self.game peekIsUp:TRUE];
    [self peekStateChange:self.peekButton];
    self.numGames++;
    
    // ((n-1)/n)*pAvg + (1/n)*currentScoreLabel
    
    double temp = self.avgScore;
    self.avgScore = (((self.numGames - 1)/self.numGames)*temp) + ((1/self.numGames)*(self.game.currentScore));
    
    for(UIButton *b in self.buttons){
        b.enabled = TRUE;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:.25];
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:b cache:YES];
        [b setTitle:@"" forState:UIControlStateNormal];
        [b setBackgroundImage:[UIImage imageNamed:@"zelda_cardback"] forState:UIControlStateNormal];
        [UIView commitAnimations];
    }
    
    int matchedCards = 0;
    for (int i = 0; i < [self.game.matchCards count]; i++) {
        RSCard *c = self.game.matchCards[i];
        if (c.matched) { matchedCards++; }
    }
    NSMutableString *s = [[NSMutableString alloc] init];
    [s appendString:@"Game Over:\n"];
    [s appendString:[NSString stringWithFormat:@"\tScore: %0.0f\n", self.game.currentScore]];
    [s appendString:[NSString stringWithFormat:@"\tCards Matched: %d\n",matchedCards]];
    [s appendString:[NSString stringWithFormat:@"\tAverage Score: %0.2f\n", self.avgScore]];
    self.game.logEntry = s;
    [self updateLog];
    
    if(!self.game.twoCardMatching){
        self.matchingModeSelector.selectedSegmentIndex = 0;
    }
    
    NSMutableArray *tempLog = self.game.logFile;
    self.game = [[CardMatchingGame alloc] initWithInt:3]; self.game.delegate = self;
    self.game.logFile = tempLog;
    [self.navBarForScore setTitle:[NSString stringWithFormat:@"Score: %0.0f", (self.game.currentScore)]];
}




-(IBAction)click:(id)sender{
    NSUInteger index = [self.buttons indexOfObject:sender];
    UIButton *b = self.buttons[index];
    NSLog(@"In the click");
    NSInteger code = [self.game buttonWasClicked:index];
    
    if (code == -1) {
        [self click:b theCard:[self.game cardFromArray:index] theBool:NO];
    }else if (code == 1){
        [self click:b theCard:[self.game cardFromArray:index] theBool:YES];
    }else if (code == 2){
        for (int i = 0; i < 30; i++) {
            RSCard *p = [self.game cardFromArray:i];
            if (p.chosen && !p.matched) {
                p.matched = true;
                [self click:self.buttons[i] theCard:p theBool:YES];
                UIButton *bu = [self.buttons objectAtIndex:i];
                bu.enabled = false;
            }else{ continue; }
        }
    }else if (code == 3){
        for (int i = 0; i < 30; i++) {
            RSCard *p = [self.game cardFromArray:i];
            if (p.chosen && !p.matched) {
                p.matched = true;
                [self click:self.buttons[i] theCard:p theBool:YES];
                UIButton *bu = [self.buttons objectAtIndex:i];
                bu.enabled = false;
            }else{ continue; }
        }
    }else{
        //this probably shouldn't happen
    }
    [self.navBarForScore setTitle:[NSString stringWithFormat:@"Score: %0.0f", (self.game.currentScore)]];
    [self updateLog];
}


- (IBAction)click:(UIButton *)sender
          theCard:(ExpandedPlayingCard *)card
          theBool:(BOOL)up {
    
    if(up){
        NSString *text = card.contents;
        NSLog(@"contents: %@", text);
        NSDictionary *dict = [ExpandedPlayingCard suitColors];
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:.25];
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:sender cache:YES];
        
        [sender setBackgroundImage:[UIImage imageNamed:@"cardfront"]
                          forState:UIControlStateNormal];
        [sender setTitle:text forState:UIControlStateNormal];
        
        if ([dict[card.suit]  isEqual: @"red"]) {
            [sender setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        }else if ([dict[card.suit] isEqual:@"blue"]){
            [sender setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        }else if ([dict[card.suit] isEqual:@"green"]){
            [sender setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
        }else{
            [sender setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        }
        
        
        [UIView commitAnimations];
    }else{
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:.25];
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:sender cache:YES];
        [sender setTitle:@"" forState:UIControlStateNormal];
        [sender setBackgroundImage:[UIImage imageNamed:@"zelda_cardback"] forState:UIControlStateNormal];
        [UIView commitAnimations];
    }
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    UIViewController *c = [segue destinationViewController];
    if (![c.title isEqual:@"testView"]) {
        self.navBarForScore.title = @"Back";
        return;
    }
    
    test *t = (test*)c;
    self.navBarForScore.title = @"Back";
    t.log = self.logFile;
    t.color = self.view.backgroundColor;
    NSLog(@"%@", [self.logFile componentsJoinedByString:@"\n"]);
}



@end
