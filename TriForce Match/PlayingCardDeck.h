//
//  PlayingCardDeck.h
//  cards
//
//  Created by Jeremy Thompson on 1/25/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import "Deck.h"

@interface PlayingCardDeck : Deck

@end
