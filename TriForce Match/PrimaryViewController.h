//
//  PrimaryViewController.h
//  TriForce Match
//
//  Created by Jeremy Thompson on 2/9/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayingCard.h"
#import "ReducedPlayingCard.h"
#import "ExpandedPlayingCard.h"
#import "CardMatchingGame.h"

@interface PrimaryViewController : UIViewController
@property (nonatomic) BOOL faceup;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttons;

- (IBAction)click:(UIButton *)sender theCard:(RSCard* )card theBool:(BOOL)up;

- (IBAction)resetWasClicked:(id)sender;
- (IBAction)peekStateChange:(id)sender;
-(void)updateLog;

@property (strong, nonatomic, readwrite) NSMutableArray *logFile;

@property (strong, nonatomic) CardMatchingGame *game;
@property (nonatomic) double numGames;
@property (nonatomic) double avgScore;
@end
