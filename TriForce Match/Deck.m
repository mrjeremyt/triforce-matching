//
//  Deck.m
//  cards
//
//  Created by Jeremy Thompson on 1/25/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import "Deck.h"

@interface Deck()
@property (strong, nonatomic) NSMutableArray *cards;
//@property (strong, nonatomic) NSMutableArray *discard;
@end


@implementation Deck

-(NSMutableArray *)cards{
    if(!_cards) _cards = [[NSMutableArray alloc] init];
//    if(!_discard) _discard = [[NSMutableArray alloc] init ];
    return _cards;
}

-(void)addCard:(Card *)card atTop:(BOOL)atTop{
    if (atTop) {
        [self.cards insertObject:card atIndex:0];
    }else{
        [self.cards addObject:card];
    }
}

-(void)addCard:(Card *)card{
    [self addCard:card atTop:NO];
}

-(NSUInteger)cardsInDeck{
    return [_cards count];
}

-(Card *)drawRandomCard{
    Card *randomCard = nil;
    
    if([self.cards count]){
        unsigned index = arc4random() % [self.cards count];
        randomCard = self.cards[index];
        [self.cards removeObjectAtIndex:index];
    }
    return randomCard;
}

-(void)shuffle{
    self.cards = [[NSMutableArray alloc] init];
}


@end
