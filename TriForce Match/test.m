//
//  test.m
//  TriForce Match
//
//  Created by Jeremy Thompson on 2/11/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import "test.h"

@implementation test

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self.textViewLog setText:[self.log componentsJoinedByString:@"\n"]];
    self.view.backgroundColor = self.color;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
