//
//  Card.h
//  cards
//
//  Created by Jeremy Thompson on 1/25/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Card : NSObject


@property (strong, nonatomic)NSString *contents;
@property (nonatomic, getter=isChosen)BOOL chosen;
@property (nonatomic, getter=isMatched)BOOL matched;


-(int)match:(NSArray *)otherCards;





@end
