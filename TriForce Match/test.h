//
//  test.h
//  TriForce Match
//
//  Created by Jeremy Thompson on 2/11/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface test : UIViewController

@property (nonatomic) NSMutableArray *log;
@property (weak, nonatomic) IBOutlet UITextView *textViewLog;
@property (nonatomic, strong) UIColor* color;


@end
