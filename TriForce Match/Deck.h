//
//  Deck.h
//  cards
//
//  Created by Jeremy Thompson on 1/25/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "card.h"

@interface Deck : NSObject

-(void)addCard:(Card *)card atTop:(BOOL)atTop;
-(void)addCard:(Card *)card;
-(NSUInteger)cardsInDeck;

-(Card *) drawRandomCard;
-(void)shuffle;



@end
