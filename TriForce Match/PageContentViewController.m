//
//  PageContentViewController.m
//  TriForce Match
//
//  Created by Jeremy Thompson on 2/9/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import "PageContentViewController.h"

@interface PageContentViewController()

@end



@implementation PageContentViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.backgroundImageView.image = [UIImage imageNamed:self.imageFile];
    self.titleLabel.text = self.titleText;
    self.titleDescription.text = self.titleDesc;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
