//
//  PlayingCard.m
//  cards
//
//  Created by Jeremy Thompson on 1/25/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import "PlayingCard.h"

@implementation PlayingCard

-(NSString *)contents{
    return [[RSCard rankStrings][self.rank] stringByAppendingString:self.suit];
}

@synthesize suit = _suit;

+ (NSArray *)validSuits{
    return @[@"♥",@"♦",@"♠",@"♣"];
}

- (void)setSuit:(NSString *)suit{
    if ([[PlayingCard validSuits] containsObject:suit]) {
        _suit = suit;
    }
}

+(NSDictionary *)suitColors{
    
    return @{@"♠":@"black",
             @"♣":@"black",
             @"♥":@"red",
             @"♦":@"red"
             };
}



+(BOOL)isRed:(PlayingCard *)card{
    if ([card.suit isEqualToString:@"♥"]) {
        return true;
    }else if([card.suit isEqualToString:@"♦"]){
        return true;
    }else{
        return false;
    }
}



- (NSString *)suit{
    return _suit ? _suit : @"?";
}


+ (NSInteger)match:(NSArray *)cards{
    NSInteger score = 0;
    NSDictionary *dict = [self suitColors];
    if ([cards count] == 2){
        PlayingCard *card1 = [cards objectAtIndex:0];
        PlayingCard *card2 = [cards objectAtIndex:1];

        if (card1.rank == card2.rank){
            if (dict[card1.suit] == dict[card2.suit]) score += 16;
            else score += 8;
        }else if([card1.suit isEqualToString:card2.suit]) score += 2;
        else if (dict[card1.suit] == dict[card2.suit]) score += 1;
    }else if ([cards count] == 3){
        NSMutableArray * pair1 = [[NSMutableArray alloc] init];
        NSMutableArray * pair2 = [[NSMutableArray alloc] init];
        NSMutableArray * pair3 = [[NSMutableArray alloc] init];
    
        [pair1 addObject:cards[0]]; [pair1 addObject:cards[1]];
        [pair2 addObject:cards[0]]; [pair2 addObject:cards[2]];
        [pair3 addObject:cards[1]]; [pair3 addObject:cards[2]];
        
        score += ([self match:pair1] + [self match:pair2] + [self match:pair3]);
    }
    return score;
}



@end
