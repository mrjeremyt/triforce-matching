//
//  CardMatchingGame.h
//  cards
//
//  Created by Jeremy Thompson on 1/29/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import "PlayingCard.h"
#import "RSCard.h"

@class PrimaryViewController;
@interface CardMatchingGame : NSObject

@property (nonatomic, readonly) BOOL twoCardMatching;
@property (nonatomic, readonly) BOOL peekUp;
@property (nonatomic, readwrite) double currentScore;
@property (nonatomic, readwrite) double prevScore;
@property (strong, nonatomic) NSMutableArray *matchCards;
@property (nonatomic, retain) PrimaryViewController *delegate;

-(instancetype)initWithInt:(int)type;

@property (nonatomic, strong) NSString* logEntry;
@property (nonatomic, strong) NSMutableArray* logFile;

-(RSCard *)cardFromArray:(NSInteger)index;
-(NSInteger)buttonWasClicked:(NSInteger)index;
-(void)changeToThreeCardMatching:(BOOL)yes;
-(void)peekIsUp:(BOOL)yes;
-(void)peek:(BOOL)yes;


@end
