//
//  CardMatchingGame.m
//  cards
//
//  Created by Jeremy Thompson on 1/29/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import "CardMatchingGame.h"
#import "PlayingCardDeck.h"
#import "ReducedPlayingCardDeck.h"
#import "ExpandedPlayingCardDeck.h"
#import "PlayingCard.h"
#import "ReducedPlayingCard.h"
#import "ExpandedPlayingCard.h"
#import "PrimaryViewController.h"


@interface CardMatchingGame()

@property (strong, nonatomic) Deck *deck;

@property (strong, nonatomic) NSMutableArray *matchedCards;
@property (nonatomic) NSInteger gameType;



@end




@implementation CardMatchingGame

-(instancetype)initWithInt:(int)type{
    self = [super init];
    if(self){
        _logFile = [[NSMutableArray alloc] init];
        _gameType = (NSInteger)type;
        
        
        if(self.gameType == 1){
            _matchCards = [[NSMutableArray alloc] init];
            _matchedCards = [[NSMutableArray alloc] init];
            self.currentScore = 0; self.prevScore = 0;
            self.deck = [[ReducedPlayingCardDeck alloc] init ];
            for(int i = 0; i < 30; i++){
                ReducedPlayingCard *pc = (ReducedPlayingCard *)[self.deck drawRandomCard];
                if(pc) {
                    [_matchCards addObject:pc];
                }
                else{
                    self = nil;
                    break;
                }
            }
        }else if (self.gameType == 2){
            _matchCards = [[NSMutableArray alloc] init];
            _matchedCards = [[NSMutableArray alloc] init];
            self.currentScore = 0; self.prevScore = 0;
            self.deck = [[PlayingCardDeck alloc] init ];
            for(int i = 0; i < 30; i++){
                PlayingCard *pc = (PlayingCard *)[self.deck drawRandomCard];
                if(pc) {
                    [_matchCards addObject:pc];
                }
                else{
                    self = nil;
                    break;
                }
            }
        }else if(self.gameType == 3){
            _matchCards = [[NSMutableArray alloc] init];
            _matchedCards = [[NSMutableArray alloc] init];
            self.currentScore = 0; self.prevScore = 0;
            self.deck = [[ExpandedPlayingCardDeck alloc] init ];
            for(int i = 0; i < 30; i++){
                ExpandedPlayingCard *pc = (ExpandedPlayingCard *)[self.deck drawRandomCard];
                if(pc) {
                    [_matchCards addObject:pc];
                }
                else{
                    self = nil;
                    break;
                }
            }
        }else{ //something else called this for some reason. just do standard PlayingCard

        }
    }
    _peekUp = false;
    _twoCardMatching = true;
    return self;
}

-(instancetype)init{
    self = [self initWithInt:-1];
    
    return self;
}




-(void)changeToThreeCardMatching:(BOOL)yes{
    if (yes) { _twoCardMatching = false; }
    else{ _twoCardMatching = true; }
    
    if([self.matchedCards count] != 0){
        for(RSCard *p in self.matchedCards){
            [self.delegate click:self.delegate.buttons[[self.matchCards indexOfObject:p]] theCard:p theBool:NO];
        }
        for(PlayingCard* p in self.matchedCards){ p.chosen = false; }
        [self.matchedCards removeAllObjects];
    }
}



-(void)peekIsUp:(BOOL)yes{
    if(yes){ _peekUp = true; }
    else{ _peekUp = false; }
}


-(void)peek:(BOOL)yes{
    int remainingCards = 0;
    for(PlayingCard *pc in self.matchedCards){
        int p = (int)[self.matchCards indexOfObject:pc];
        UIButton *b = [self.delegate.buttons objectAtIndex:p];
        if(yes){
            b.enabled = false;
            b.backgroundColor = [UIColor colorWithWhite:1.0 alpha:1.0];
        }else{
            b.enabled = true;
            b.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.0];
        }
    }
    
    for(int i = 0; i < [self.delegate.buttons count]; i++){
        RSCard *p = [self cardFromArray:i];
        if(!p.chosen){
            UIButton *b = [self.delegate.buttons objectAtIndex:i];
            remainingCards++;
            if(yes){
                [self.delegate click:b theCard:p theBool:YES];
                b.enabled = false;
                b.backgroundColor = [UIColor colorWithWhite:1.0 alpha:1.0];
            }else{
                [self.delegate click:b theCard:p theBool:NO];
                b.enabled = true;
                b.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.0];
            }
        }
    }
    if (yes) {
        self.logEntry = [NSString stringWithFormat:@"Peek Action: %d cards have not yet been matched", remainingCards];
    }
}


-(RSCard *)cardFromArray:(NSInteger)index{
    return _matchCards[index];
}



-(NSInteger)buttonWasClicked:(NSInteger)index{
    PlayingCard *card = [self.matchCards objectAtIndex:index];
    
    if([self.matchedCards containsObject:card]){
        card.chosen = false;
        [self.matchedCards removeObject:card];
        return -1;
    }
    [self.matchedCards addObject:card];
    card.chosen = true;
    self.currentScore -= 1;
    
    if (([self.matchedCards count] == 1) || (([self.matchedCards count] == 2) && self.twoCardMatching == FALSE)) {
        return 1;
    }else if (([self.matchedCards count] == 2) && (self.twoCardMatching == TRUE)){
        self.prevScore = [PlayingCard match:self.matchedCards];
        self.currentScore += self.prevScore;

        //diable and grey out the buttons, which is done on return
        //empty the array
        [self makeLogEntry:self.matchedCards theScore:self.prevScore];
        [self.matchedCards removeAllObjects];
        return 2;
    }else{//have to check 3 card matching
        self.prevScore = [PlayingCard match:self.matchedCards];
        self.currentScore += self.prevScore;

        //diable and grey out the buttons, which is done on return
        //empty the array
        [self makeLogEntry:self.matchedCards theScore:self.prevScore];
        [self.matchedCards removeAllObjects];
        return 3;
    }
}


-(void)makeLogEntry:(NSMutableArray *)cards theScore:(NSInteger)score{
    NSMutableString *s = [[NSMutableString alloc] init];
    [s appendString:@"•Match of ("];
    for (int i = 0; i < [cards count]; i++) {
        RSCard *c = cards[i];
        [s appendString:[NSString stringWithFormat:@"%@", c.contents]];
        if(i != [cards count] - 1) [s appendString:@", "];
        else [s appendString:@") "];
    }
    [s appendString:[NSString stringWithFormat:@" is worth %ld points", (long)score]];
    self.logEntry = s;
    NSLog(@"logEntry: %@", s);
}



@end
