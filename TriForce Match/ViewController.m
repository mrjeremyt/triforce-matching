//
//  ViewController.m
//  TriForce Match
//
//  Created by Jeremy Thompson on 2/9/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import "ViewController.h"


@interface ViewController()
@property (nonatomic, strong) NSString* welcome;
@property (nonatomic, strong) NSString* reduced;
@property (nonatomic, strong) NSString* standard;
@property (nonatomic, strong) NSString* extended;

@property (nonatomic, strong) NSString* welcome_desc;
@property (nonatomic, strong) NSString* reduced_desc;
@property (nonatomic, strong) NSString* standard_desc;
@property (nonatomic, strong) NSString* extended_desc;
@end


@implementation ViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self instantiate];
    
    
    // Create the data model
    _pageTitles = @[_welcome, _reduced, _standard, _extended];
    _pageDescriptions = @[_welcome_desc, _reduced_desc, _standard_desc, _extended_desc];
    _pageImages = @[@"loz_rules_bg_2.png", @"loz_rules_bg_2.png", @"loz_rules_bg_2.png", @"loz_rules_bg_2.png"];
    
    // Create page view controller
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.pageViewController.dataSource = self;
    
    PageContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (PageContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.pageTitles count] == 0) || (index >= [self.pageTitles count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    PageContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageContentController"];
    pageContentViewController.imageFile = self.pageImages[index];
    pageContentViewController.titleDesc = self.pageDescriptions[index];
    
    pageContentViewController.titleText = self.pageTitles[index];
    pageContentViewController.pageIndex = index;
    
    return pageContentViewController;
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self.pageTitles count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.pageTitles count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}

-(void)instantiate{
    _welcome = @"TriForce Matching";
    _reduced = @"Reduced Matching";
    _standard = @"Standard Matching";
    _extended = @"Extended Matching";
    
    _welcome_desc = @"TriForce Matching is a card matching game with 30 cards. You can get 1 point for a match of the same color, 2 points for a the same suit, 8 points for the same rank, 16 for the same rank and color, and 32 points for an identical card match. There is a -1 point penalty for each time you flip a card up, and a -15 penalty if you decide to peek at all the cards. You can have 2 or 3 card matching mode. In 3 card, you match together 3 cards and they are scored as 3 2-card pairs. There are 3 versions of the game so swipe right for the rules of each.";
        
    _reduced_desc = @"This version of the game is all spades, so there are 4 of each card. That means it is more likely to hit the full 32 points for matches. ";
    _standard_desc = @"Standard is a typical game. There are the normal 4 suits and as such it will be impossible to have an identical match. The highest score for this game is 16 for a 2 card match, and 32 points total. ";
    _extended_desc = @"Extended gives you 4 additional suits for a total of 104 cards. You now have the Sun, Moon, Turnip, and Trident suits. ";
}





@end
