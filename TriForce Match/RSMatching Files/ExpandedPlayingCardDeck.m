//
//  ExpandedPlayingCardDeck.m
//  TriForce Match
//
//  Created by Jeremy Thompson on 2/11/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import "ExpandedPlayingCardDeck.h"
#import "ExpandedPlayingCard.h"

@implementation ExpandedPlayingCardDeck

-(instancetype) init{
    
    self = [super init];
    
    
    if(self){
        for (NSString *suit in [ExpandedPlayingCard validSuits]){
            for (NSUInteger rank = 1; rank <= [ExpandedPlayingCard maxRank]; rank++){
                ExpandedPlayingCard *card = [[ExpandedPlayingCard alloc] init];
                card.rank = rank;                
                card.suit = suit;
                [self addCard:card];
            }
        }
    }
    return self;
}


@end
