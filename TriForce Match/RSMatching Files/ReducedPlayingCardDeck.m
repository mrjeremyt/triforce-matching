//
//  ReducedPlayingCardDeck.m
//  TriForce Match
//
//  Created by Jeremy Thompson on 2/11/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import "ReducedPlayingCardDeck.h"
#import "ReducedPlayingCard.h"

@implementation ReducedPlayingCardDeck

-(instancetype) init{
    
    self = [super init];
    
    
    if(self){
        for (NSString *suit in [ReducedPlayingCard validSuits]){
            for (NSUInteger rank = 1; rank <= [ReducedPlayingCard maxRank]; rank++){
                ReducedPlayingCard *card = [[ReducedPlayingCard alloc] init];
                card.rank = rank;
                card.suit = suit;
                [self addCard:card];
            }
        }
    }
    return self;
}


@end
