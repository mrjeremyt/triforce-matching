//
//  RSCard.h
//  TriForce Match
//
//  Created by Jeremy Thompson on 2/11/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import "Card.h"

@interface RSCard : Card

+(NSArray *)validSuits;
+(NSDictionary *)suitColors;
@property (strong, nonatomic) NSString *suit;
@property (nonatomic)NSUInteger rank;
@property (nonatomic) BOOL isFaceUp;


+(NSArray *)rankStrings;
+(NSUInteger)maxRank;
+(NSInteger)match:(NSArray *)cards;

@end
