//
//  ExpandedPlayingCardDeck.h
//  TriForce Match
//
//  Created by Jeremy Thompson on 2/11/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import "Deck.h"

@interface ExpandedPlayingCardDeck : Deck

@end
