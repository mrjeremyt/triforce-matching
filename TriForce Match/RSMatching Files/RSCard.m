//
//  RSCard.m
//  TriForce Match
//
//  Created by Jeremy Thompson on 2/11/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import "RSCard.h"

@implementation RSCard


+(NSArray *)validSuits{
    return nil;
}

+(NSDictionary *)suitColors{
    return nil;
}


+ (NSArray *)rankStrings{
    return @[@"?",@"A",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"J",@"Q",@"K"];
}

+(NSUInteger)maxRank{ return [[self rankStrings] count] -1; }


- (void)setRank:(NSUInteger)rank{
    if (rank <= [RSCard maxRank]){ _rank = rank;  }
}



+(NSInteger)match:(NSArray *)cards{
    return -1;
}


@end
