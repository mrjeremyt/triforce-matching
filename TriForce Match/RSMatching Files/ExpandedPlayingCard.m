//
//  ExpandedPlayingCard.m
//  TriForce Match
//
//  Created by Jeremy Thompson on 2/11/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import "ExpandedPlayingCard.h"

@implementation ExpandedPlayingCard
-(NSString *)contents{
    //    NSArray *rankStrings = [PlayingCard rankStrings];
    return [[ExpandedPlayingCard rankStrings][self.rank] stringByAppendingString:self.suit];
}

@synthesize suit = _suit;

+ (NSArray *)validSuits{
    return @[@"♥",@"♦",@"♠",@"♣",
             @"☼", @"☾", @"♆", @"❧"];
}

- (void)setSuit:(NSString *)suit{
    if ([[ExpandedPlayingCard validSuits] containsObject:suit]) {
        _suit = suit;
    }
}


+(NSDictionary *)suitColors{
    
    return @{@"♠":@"black",
             @"♣":@"black",
             @"♥":@"red",
             @"♦":@"red",
             @"☼":@"blue",
             @"☾":@"blue",
             @"♆":@"green",
             @"❧":@"green"
             };
}


+(BOOL)isRed:(ExpandedPlayingCard *)card{
    if ([card.suit isEqualToString:@"♥"]) {
        return true;
    }else if([card.suit isEqualToString:@"♦"]){
        return true;
    }else{
        return false;
    }
}



- (NSString *)suit{
    return _suit ? _suit : @"?";
}


+ (NSInteger)match:(NSArray *)cards{
    NSInteger score = 0;
    NSDictionary *dict = [self suitColors];
    if ([cards count] == 2){
        ExpandedPlayingCard *card1 = [cards objectAtIndex:0];
        ExpandedPlayingCard *card2 = [cards objectAtIndex:1];
        
        if (card1.rank == card2.rank){
            if (dict[card1.suit] == dict[card2.suit]) score += 16;
            else score += 8;
        }else if([card1.suit isEqualToString:card2.suit]) score += 2;
        else if (dict[card1.suit] == dict[card2.suit]) score += 1;
    }else if ([cards count] == 3){
        NSMutableArray * pair1 = [[NSMutableArray alloc] init];
        NSMutableArray * pair2 = [[NSMutableArray alloc] init];
        NSMutableArray * pair3 = [[NSMutableArray alloc] init];
        
        [pair1 addObject:cards[0]]; [pair1 addObject:cards[1]];
        [pair2 addObject:cards[0]]; [pair2 addObject:cards[2]];
        [pair3 addObject:cards[1]]; [pair3 addObject:cards[2]];
        
        score += ([self match:pair1] + [self match:pair2] + [self match:pair3]);
    }
    return score;
}
@end
