//
//  ReducedPlayingCard.m
//  TriForce Match
//
//  Created by Jeremy Thompson on 2/11/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import "ReducedPlayingCard.h"
#import "SecondViewController.h"

@interface ReducedPlayingCard()
@property (nonatomic) NSString* logEntry;
@end

@implementation ReducedPlayingCard
-(NSString *)contents{
    //    NSArray *rankStrings = [PlayingCard rankStrings];
    return [[RSCard rankStrings][self.rank] stringByAppendingString:self.suit];
}

@synthesize suit = _suit;

+ (NSArray *)validSuits{
    return @[@"♠", @"♠", @"♠", @"♠"];
}

- (void)setSuit:(NSString *)suit{
    if ([[ReducedPlayingCard validSuits] containsObject:suit]) {
        _suit = suit;
    }
}


+(BOOL)isRed:(ReducedPlayingCard *)card{
    if ([card.suit isEqualToString:@"♥"]) {
        return true;
    }else if([card.suit isEqualToString:@"♦"]){
        return true;
    }else{
        return false;
    }
}

+(NSDictionary *)suitColors{
    
    return @{@"♠":@"black"};
}


- (NSString *)suit{
    return _suit ? _suit : @"?";
}



+ (NSInteger)match:(NSArray *)cards{
    NSInteger score = 0;
    if ([cards count] == 2){
        ReducedPlayingCard *card1 = [cards objectAtIndex:0];
        ReducedPlayingCard *card2 = [cards objectAtIndex:1];
        
        if (card1.rank == card2.rank){
            score += 32;
        }else{ score += 2; }
    }else if ([cards count] == 3){
        NSMutableArray * pair1 = [[NSMutableArray alloc] init];
        NSMutableArray * pair2 = [[NSMutableArray alloc] init];
        NSMutableArray * pair3 = [[NSMutableArray alloc] init];
        
        [pair1 addObject:cards[0]]; [pair1 addObject:cards[1]];
        [pair2 addObject:cards[0]]; [pair2 addObject:cards[2]];
        [pair3 addObject:cards[1]]; [pair3 addObject:cards[2]];
        
        score += ([self match:pair1] + [self match:pair2] + [self match:pair3]);
    }
    return score;
}
@end
